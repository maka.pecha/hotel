
function validar(telefono, fecha){
  console.log(fecha, 'fecha');
  if(!telefono || !fecha ){
    alert("Algunos campos estan vacios!");
    return false;
  }else{
    alert("Su consulta ha sido registrada correctamente.");
    return true;
  }
}
function guardar(){
  let telefono = document.getElementById('telefono').value;
  let nombre = document.getElementById('nombre').value;

  validar(telefono, nombre);
}
// Enunciado
// Una inmobiliaria recibe muchas consultas telefónicas referidas a las propiedades que tiene ofrecidas para alquiler.
// Para simplificar el proceso nos pidió un sitio web donde puedan publicar información de las propiedades y en el que
// los interesados puedan comunicarse para realizar consultas o concertar una visita.
//
//   El sitio web debe poseer las siguientes páginas:
//
//   Inicio: debe mostrar información general de la inmobiliaria incluyendo al menos una foto.
//   Galería de propiedades: debe mostrar una tabla con información de las casas que tienen en alquiler, incluyendo una
//   foto de cada una de ellas.
//   Formulario de consultas: debe permitir ingresar los siguientes datos:
//   - Nombre
//
//   - Teléfono
//
//   - Tipo de consulta, donde se pueda seleccionar entre "Visita","Búsqueda" o "Consulta general"
//
// - Fecha
//
//
//
// En el formulario se deben utilizar los controles más adecuados para cada dato. El formulario debe validarse teniendo
// en cuenta que son obligatorios únicamente el nombre y el teléfono, pero que si selecciona "Visita", también debe
// ingresarse la fecha. Si el formulario se carga correctamente, se debe presentar un mensaje con el texto "Muchas
// gracias, NOMBRE, lo llamaremos al TELEFONO", reemplazando NOMBRE y TELEFONO por los datos ingresados.
//
//   El diseñador gráfico ya redactó todos los archivos HTML, y por lo tanto nos solicita que UNICAMENTE programemos la
//   funcionalidad del formulario en javascript, agregándola a la siguiente página. El diseñador no nos autoriza ningún
//   tipo de modificación en el diseño gráfico de la misma, por lo tanto sólo podremos agregar los controles faltantes
//   del formulario, el código javascript y etiquetas y atributos no visibles (form, id, onsubmit, etc).
//
//
//
// <html>
//
// <head>
//
// <title>Formulario de consultas</title>
//
// </head>
//
//
//
// <body>
//
//
//
// <h1>Formulario de consultas</h1>
//
//
//
// <table>
//
// <tr>
//
// <td>Nombre</td>
//
// <td><input type=""></td>
//
//   </tr>
//
//   <tr>
//
//   <td>Teléfono</td>
//
//   <td></td>
//
//   </tr>
//
//   <tr>
//
//   <td>Tipo de consulta</td>
//
// <td></td>
//
// </tr>
//
// <tr>
//
// <td>Fecha</td>
//
// <td></td>
//
// </tr>
//
// <tr>
//
// <td><br /></td>
//
// <td><input type="submit"></td>
//
//   </tr>
//
//   </table>
//
//   </head>
//
//   </body>
//
//
//
//   </html>